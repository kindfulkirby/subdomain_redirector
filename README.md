Subdomain Redirector
====================

## What it does

Imagine you have a funny domain name like `suc.ks`, and you want to have subdomains for everything that sucks, that
redirect to rants, alternatives or disparaging memes. You could create a CNAME record for every single one, but then
you would have to log into your registrars clunky webinterface (with two-factor authentication, of course, safety
first!) *every time* you want to vent your frustration.

Or you could just have an A record for `*.suc.ks` point to this little server (usually proxied behind a webserver) and
manage just a single, user-editable file that looks like this:

```
xkcd: http://www.explainxkcd.com/
blackmesa: http://www.aperturescience.com/
```

Now `http://xkcd.suc.ks` (it doesn't, it's just an example!) redirects every visitor to
http://www.explainxkcd.com/ and `http://blackmesa.suc.ks` (harsh but true!) redirects to their clearly superior
competitor at http://www.aperturescience.com/.

For a real world example: This software is availabe at http://cname.hurtsmybra.in/!

## Installation & configuration

Make sure you have Flask and PyYAML installed, either on your system, or - preferably - in a virtualenv.

Edit `main.py` and change the `domain` variable to whatever your domain is, make sure to seperate the elements at the
`.`s.  
Change the `port` variable if you want to. Make sure your webserver proxies the (sub)domain to it, or just set it to port
80, but then you probably need to run this as root, which is a bad idea.

You might also want to take a look at `templates/default.html`, which is displayed when the called subdomain is not
configured or the domain is called without a subdomain.

**Do NOT point any other domains at this, it will break!**

## Running

Run it with `python main.py`.
